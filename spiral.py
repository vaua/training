n = 5
m = [[0 for i in range(n)] for i in range(n)]
m[n // 2][n // 2] = n ** 2
step, v = 1, 0
for ran in range(n-1, -1, -2):

    for i in range(ran):
        m[0+v][i+v] = step
        step += 1
    for j in range(ran):
        m[j+v][ran+v] = step
        step += 1
    for k in range(ran):
        m[ran+v][ran-k+v] = step
        step += 1
    for l in range(ran):
        m[ran-l+v][0+v] = step
        step += 1
    v += 1

for i in m:
    for j in i:
        print(j, end=' ')
    print('')
