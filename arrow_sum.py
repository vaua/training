def sq(matrix, n):
    if n < 1:
        return "Matrix size must be more than 0"
    elif n == 1:
        return matrix[n-1][n-1]
    else:
        summa = matrix[n-1][n-1]
        for i in range(n-1):
            summa += matrix[i][i] + matrix[i][n-1] + matrix[n - 1][i]
        return summa

t = [[1, 1, 0, 0, 0],
    [1, 1, 0, 0, 1],
    [1, 0, 1, 0, 1],
    [1, 0, 0, 0, 1],
    [1, 1, 0, 1, 1], ]


assert sq(t,5) == 10
assert sq(t,2) == 4
assert sq(t,1) == 1