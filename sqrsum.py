def sq(matrix, n):
    if n < 1:
        return "Matrix size must be more than 0"
    elif n == 1:
        return matrix[0][0]
    else:
        summa = 0
        for i in range(0, n-1):
            summa += matrix[0][i] + matrix[i][n - 1] + matrix[n - 1][n - i - 1] + matrix[n - i - 1][0]
        return summa



t = [[1, 1, 0, 0, 0],
      [1, 1, 0, 0, 1],
      [1, 0, 1, 0, 1],
      [1, 0, 0, 0, 1],
      [1, 1, 0, 1, 1], ]


assert sq(t, 5) == 12
assert sq(t, 2) == 4
assert sq(t, 1) == 1
assert sq(t, 3) == 5
